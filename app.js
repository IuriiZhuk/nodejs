let express = require('express');
let bodyParser = require('body-parser');

let app = express();

app.use(bodyParser.json());

let users = [
  {
    id: 1,
    name: 'User1'
  },
  {
    id: 2,
    name: 'User2'
  },
  {
    id: 3,
    name: 'User3'
  }
];

// GET
app.get('/', function(req, res){
  res.send('Hello world!');
});

app.get('/users', function (req, res) {
  res.send(users);
});

app.get('/users/:id', function (req, res) {

  let user = users.find(function (user) {
  return user.id === parseInt(req.params.id);
})
  
  res.send(user);
});

// POST

app.post('/users', function(req,res){
  let user = {
    id : Date.now(),
    name : req.body.name
  }
  users.push(user);
  console.log(users);
  res.send(user);
})


//PUT

app.put('/users/:id', function(req,res){
  let user = users.find(function(user){
    return user.id === parseInt(req.params.id);
  });

  user.name = req.body.name;
  res.send(user);
})


//DELETE

app.delete('/users/:id', function(req,res){
  users = users.filter(function(user){
    return user.id !== parseInt(req.params.id)
  });
  res.sendStatus(200);
})


app.listen(3000, function(){
  console.log('App listening on port 3000!');


});